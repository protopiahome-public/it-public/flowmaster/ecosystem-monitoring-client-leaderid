<?php

error_reporting(E_ALL);

include("config.php");
require_once(__DIR__ . "/classes.php");

$_GET = mock($_GET, "");
$_POST = mock($_POST, "");
$_REQUEST = mock($_REQUEST, "");
$_COOKIE = mock($_COOKIE, "");

$ecosystem_token = ($_COOKIE["token"]);

$current_user = "";

$isset2 = function(&$a, $b = null){return isset2($a, $b);};

function show_header()
{
	global $ecosystem_token, $current_user;
	
	echo <<<EOF
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="bootstrap.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="jquery-1.12.4.js"></script>
  <script src="jquery-ui.js"></script>
  <script src="bootstrap.min.js"></script>
  <script>
  $( function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  </script>
  </head>
  
    <style>
    @import url("https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css");
    body {
      background-color: #fafafa;
      color: #111;
      font-size: 12pt;
      font-family: "Helvetica", sans-serif;
    }

    header {
       margin-bottom: 15px;
      padding: 15px;
      min-height: 30px;
      background: 
    }

    footer {
      margin-top: 15px;
      padding: 15px;
      min-height: 50px;
    }
    .left-menu-item a {
      color: black;
      text-decoration: none;
	  display: block;
    }

     th {
padding: 10px 20px;
background: #56433D;
color: #F9C941;
border-right: 2px solid; 
font-size: 0.9em;
}
    td {
      padding: 5px;
    }
    .left-menu {
     
        border-radius: 15px;
    }

    .left-menu-item {
      padding: 5px;
      border-radius: 5px;
       color: #666;
       margin-right: 20px;
	   /*
       background-color: #ffb; 
       border-bottom: 1px solid rgba(255, 118, 0, 0.28);
	   */
    margin-top: 8px;
    text-indent: 25px;
    margin-left: 15px;
    font-size: 14pt;
    }
    .top-menu-item {
      margin-top: 14px;
        font-size: 14pt;
        font-weight: bold;

    }

    .left-menu-item:hover {
      background-color: #eea; 
    }

    .input-line {
      margin-top: 7px;
      margin-bottom: 8px;
    }

    .input-left {
      display: inline-block;
      width: 40%;
      font-weight: bold;
      text-align: right;
    }

    .input-right {
      display: inline-block;
    }

    .form-depe {
      text-align: center;
    }

    .team {
      cursor: pointer;
      text-align: center;
      font-weight: bold;
      padding: 5px;
      color: white;
      background-color: #428bca;
      border-radius: 5px;
      margin-top: 7px;
    }
/*
@media screen and (max-width: 640px) {
	table {
		border-width: 0px;
	}
	td {
		display: block;
	}
	tr {
		border-width: 0px;
		display: block;
		padding: 10px 0px;
	}
}
*/
  </style>
  
<body>
<header></header>
 <div class="container">
        <div class="row">
        <div class="col-md-4">
 <h1>Чат-мониторинг</h1>
EOF;
	if ($ecosystem_token)
	{
		$current_user = protopia_query("getCurrentUser", "_id email name roles");

		$current_user["role"] = $current_user["roles"][0];
		$menu_items = [];

		$current_user["role"] = $current_user["roles"][0];
		echo <<<EOF
<div class="left-menu">
<div class="top-menu-item">Вы {$current_user["name"]} ({$current_user["role"]}) | <a href="?action=logout">Выйти</a></div>
EOF;
		foreach ($menu_items as $menu_action => $menu_item)
		{
			echo "<div class=\"left-menu-item\"><a href=\"?action={$menu_action}\">{$menu_item}</a></div>";
		}
		echo "</div>";
	}
	
	echo "</div><div class=\"col-md-8\">";
}

function show_footer()
{
	echo <<<EOF
</div>
</div>
</div>
<footer></footer>
</body>
</html>
EOF;
}

$actions = [];
$action_files = scandir(__DIR__ . "/actions");
foreach($action_files as $action_file)
{
	if (preg_match("#^(.*)\.php$#", $action_file, $matches))
	{
		$actions[] = $matches[1];
	}
}

$action = ($_REQUEST["action"]);

if (!$ecosystem_token && !in_array($action, ["auth", "register_form", "register"]))
{
	show_header();
	include(__DIR__ . "/actions/auth_form.php");
	show_footer();
}
elseif(in_array($action, $actions))
{
	if (preg_match("#_form$#", $action))
	{
		show_header();
	}
	include(__DIR__ . "/actions/{$action}.php");
	if (preg_match("#_form$#", $action))
	{
		show_footer();
	}
}
else
{
	show_header();
	show_footer();
}

function getDropDown($name, $query, $fields = ["title"], $selected = null)
{
	$result = "";
	
	$data = protopia_query($query, "_id " . implode(" ", $fields));
	
	$result .= "<select name=\"{$name}\">";
	foreach ($data as $element)
	{
		$title = "";
		foreach ($fields as $field)
		{
			$title .= htmlspecialchars($element[$field]) . " ";
			$selected_html = ($selected && $element["_id"] == $selected) ? "selected" : "";
		}
		$result .= "<option {$selected_html} value=\"{$element["_id"]}\">{$title}</option>";
	}
	$result .= "</select>";
	return $result;
}

function getCustomDropDown($name, $fields, $selected = "")
{
	$result = "";
	
	$result .= "<select name=\"{$name}\">";
	foreach ($fields as $k => $v)
	{
		$k = htmlspecialchars($k);
		$v = htmlspecialchars($v);
		$selected_html = $k == $selected ? "selected" : "";
		$result .= "<option {$selected_html} value=\"{$k}\">{$v}</option>";
	}
	$result .= "</select>";
	return $result;
}

function getCheckBox($name, $value, $title)
{
	$checked = $value ? "checked=\"checked\"" : "";
	return <<<EOF
	<label for="{$name}">{$title}: </label><input name={$name} value="1" type=checkbox {$checked}/>
EOF;
}

function isset2(&$var, $default = null)
{
	return $var ? $var : $default;
}

function protopia_query($method, $results, $params_variables = null)
{
	$variables = [];
	if ($params_variables)
	{
		foreach ($params_variables as $key => $value)
		{
			$key = trim($key);
			$key = explode(":", $key);
			$key[0] = trim($key[0]);
			$key[1] = trim($key[1]);
			$variables[$key[0]] = $value;
			$params1[] = "$" . $key[0] . ":" . $key[1];
			$params2[] = $key[0] . ": $" . $key[0];
		}
		$params1 = implode(", ", $params1);
		$params2 = implode(", ", $params2);
	
		$query = <<<EOF
query ({$params1}) {
	{$method} ({$params2}) {
		{$results}
	}
}
EOF;
	}
	else
	{
		$query = <<<EOF
query {
	{$method} {
		{$results}
	}
}
EOF;
	}
	//echo $query;
	return protopia_graphql($query, $variables);
}

function protopia_mutation($method, $results, $params_variables = null)
{
	$variables = [];
	foreach ($params_variables as $key => $value)
	{
		$key = trim($key);
		$key = explode(":", $key);
		$key[0] = trim($key[0]);
		$key[1] = trim($key[1]);
		$variables[$key[0]] = $value;
		$params1[] = "$" . $key[0] . ":" . $key[1];
		$params2[] = $key[0] . ": $" . $key[0];
	}
	$params1 = implode(", ", $params1);
	$params2 = implode(", ", $params2);
	
	$query = <<<EOF
mutation ({$params1}) {
	{$method} ({$params2}) {
		{$results}
	}
}
EOF;
	return protopia_graphql($query, $variables);
}

function protopia_graphql($query, $variables = [])
{
	global $ecosystem_token, $ecosystem_addr;
	
	return graphql_query($ecosystem_addr, $query, $variables, $ecosystem_token);
}

function graphql_query($endpoint, $query, $variables = [], $token = null)
{
	global $ecosystem_client_secret;
	
    $headers = ['Content-Type: application/json', 'User-Agent: Dunglas\'s minimal GraphQL client'];
    if ($token) {
        $headers[] = "Authorization: Bearer $token";
    }
	else {
		$headers[] = "Authorization: Basic {$ecosystem_client_secret}";
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$endpoint);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['query' => $query, 'variables' => $variables]));
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$data = curl_exec($ch);
	curl_close ($ch);
    $result = json_decode($data, true);

    if (isset($result["errors"])) {
		
		echo "<!--";
		print_r($headers);
		print_r(['query' => $query, 'variables' => $variables]);
		foreach ($result["errors"] as $error)
		{
			echo $error["message"] . "\n";
		}
		print_r($result);
		echo "-->";
		return $result;
		die();
    }
	$result = $result["data"];
	return reset($result);
}