<?php

class mockClass implements ArrayAccess {
    private $container = array();
	private $default = null;

    public function __construct($init = array(), $default) {
        $this->container = $init;
		$this->default = $default;
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : $this->default;
    }
}

function mock($init = [], $default = null)
{
	return new mockClass($init, $default);
}

function htmlspecialchars_recursive($array)
{
	foreach ($array as $k => &$v)
	{
		if (is_array($v))
		{
			$v = htmlspecialchars_recursive($v);
		}
		else
		{
			$v = htmlspecialchars($v);
		}
	}
	
	return $array;
}