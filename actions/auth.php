<?php

	/* @var $LOC array */

	if (encode($_REQUEST[ 'code' ]) != '')
	{
		$params = [
			'client_id'     => SSO_APP_ID,
			'client_secret' => SSO_APP_SECRET,
			'code'          => $_REQUEST[ 'code' ],
			'redirect_uri'  => $server_absolute_path . 'sso_auth/',
			'grant_type'    => 'authorization_code'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, SSO_APP_URL . '/oauth2/access_token');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$html = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($html, true);

		if (isset($response[ 'access_token' ]))
		{
			$token_type = isset($response[ 'token_type' ]) ? $response[ 'token_type' ] : 'Bearer';

			$_SESSION[ 'sso_access_token' ] = $response[ 'access_token' ];

			$params = [
				'access_token' => $_SESSION[ 'sso_access_token' ],
				'code'         => $_REQUEST[ 'code' ]
			];

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, SSO_APP_URL . '/users/me?' . http_build_query($params));
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, [ "Authorization: $token_type " . $_SESSION[ 'sso_access_token' ] ]);
			$html = curl_exec($ch);
			curl_close($ch);
			$response = json_decode($html, true);


			
					
				$id_token = array(
				"iss" => "leader-id.ru",
				"sub" => $response[ 'leader_id' ],
				"aud" => "",
				"exp" => "",
				"nonce" => "",
				"acr" => "leader_id",
			);
			$jwt = JWT::encode($id_token, $jwt_key);

			$result = protopia_mutation("authorize", "token", ["input: AuthorizeInput!" => 
			array(
				"response_type" => "token",
				"client_id" => $ecosystem_client_id,
				"scope" => "user",
				"login_hint_token" => $jwt,
				)
			]);
			if (!$result["token"])
			{
				protopia_mutation("registerUser", "_id", ["input: UserInput" => [
					"name" => $response[ 'firstname' ],
					"family_name" => $data["message"]["from"]["last_name"],
					"leaderid_id" => $response[ 'lastname' ]
				]]);
				$result = protopia_mutation("authorize", "token", ["input: AuthorizeInput!" => 
			array(
				"response_type" => "token",
				"client_id" => $ecosystem_client_id,
				"scope" => "user",
				"login_hint_token" => $jwt,
				)
			]);
			}
			if (isset($result["errors"])){
					echo "<p>Ошибка авторизации.</p>";
					die();
			}
			setcookie("token", $result["token"], time()+60*60*24*6004, "/");
			header("Location: ?");
		}
	
		else
		{
			$auth_error = true;
		}
	}
	elseif (encode($_REQUEST[ 'error_description' ]) != '')
	{
		$auth_error = true;
	}
	else
	{
		$content2 .= '<div class="maincontent_data">
<div class="soc_part full">
...
' . $LOC[ 'redirect_to_auth' ] . '
</div>
</div>
<script>
$(document).ready(function(){
	window.location = "' . SSO_APP_URL . '/oauth2/authorize?response_type=code&grant_type=authorization_code&client_id=' . SSO_APP_ID . '&redirect_uri=' . $server_absolute_path . 'sso_auth/";
});
</script>';
	}