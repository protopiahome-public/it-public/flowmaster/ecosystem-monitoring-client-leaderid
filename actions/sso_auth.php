<?php
	/* @var $LOC array */

	$auth_error = false;
	$auth_error_description = '';

	if (!is_logged())
	{
		if (encode($_REQUEST[ 'code' ]) != '')
		{
			$params = [
				'client_id'     => SSO_APP_ID,
				'client_secret' => SSO_APP_SECRET,
				'code'          => $_REQUEST[ 'code' ],
				'redirect_uri'  => $server_absolute_path . 'sso_auth/',
				'grant_type'    => 'authorization_code'
			];

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, SSO_APP_URL . '/oauth2/access_token');
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$html = curl_exec($ch);
			curl_close($ch);
			$response = json_decode($html, true);

			if (isset($response[ 'access_token' ]))
			{
				$token_type = isset($response[ 'token_type' ]) ? $response[ 'token_type' ] : 'Bearer';

				$_SESSION[ 'sso_access_token' ] = $response[ 'access_token' ];

				$params = [
					'access_token' => $_SESSION[ 'sso_access_token' ],
					'code'         => $_REQUEST[ 'code' ]
				];

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, SSO_APP_URL . '/users/me?' . http_build_query($params));
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, [ "Authorization: $token_type " . $_SESSION[ 'sso_access_token' ] ]);
				$html = curl_exec($ch);
				curl_close($ch);
				$response = json_decode($html, true);


				if ($response[ 'username' ] != '')
				{
					$_SESSION[ 'sso_email' ] = $response[ 'email' ];

					$check_user = fetch_array(query('SELECT * FROM user WHERE sso="' . $response[ 'username' ] . '" OR em="' . encode($_SESSION[ 'sso_email' ]) . '"'));
					if ($check_user[ 'id' ] != '')
					{
						$_SESSION[ 'user_id' ] = $check_user[ 'id' ];
						$_SESSION[ 'allrights' ] = $check_user[ "rights" ];
                        // by Lea_alex
                        $_SESSION['role'] = $check_user["role"];

						setcookie("user_id", $check_user[ 'id' ], time() + 60 * 60 * 24 * 30, '/', $cookiedomain);
						setcookie("pass", $check_user[ 'pass' ], time() + 60 * 60 * 24 * 30, '/', $cookiedomain);

						$redirect_path = create_redirect();
						if ($redirect_path != '')
						{
							redirect($redirect_path);
						}
						else
						{
							redirect($server_absolute_path . 'socstart/');
						}
					}
					else
					{
						$check_user = fetch_array(query('SELECT * FROM user WHERE em="' . encode($_SESSION[ 'sso_email' ]) . '"'));
						if (encode($_SESSION[ 'sso_email' ]) != '' && $check_user[ 'id' ])
						{
							$auth_error = true;
							$auth_error_description = $LOC[ 'email_already_registered' ];
						}
						else
						{
							$bdate = '';
							if (isset ($response[ 'meta' ][ 'date_of_birth' ]) && preg_match('#(\d+)\.(\d+).(\d+)#', encode($response[ 'meta' ][ 'date_of_birth' ])))
							{
								$bdate = '"' . date("Y-m-d", strtotime($response[ 'meta' ][ 'date_of_birth' ])) . '"';
							}
							else
							{
								$bdate = "NULL";
							}

							$name = trim(( isset($response[ 'firstname' ]) ? encode($response[ 'firstname' ]) : '' ) . ' '
								. ( isset($response[ 'lastname' ]) ? encode($response[ 'lastname' ]) : '' ));

							query('INSERT INTO user (login,em,fio,birth,sso,leader_id,unti_id,subs_type,subs_objects,updated_at,created_at) VALUES ("' . encode($_SESSION[ 'sso_email' ]) . '","' . encode($_SESSION[ 'sso_email' ]) . '","' . $name . '",' . $bdate . ',"' . encode($response[ 'username' ]). '","' .encode($response[ 'leader_id' ]) .'","' .encode($response[ 'unti_id' ]) .'",1,"' . getSubsObjectsList() . '",' . time() . ',' . time() . ')');
							$id = m_insert_id();

							if ($id > 0)
							{
								unset($_SESSION[ 'sso_email' ]);

								$_SESSION[ 'user_id' ] = $id;
								setcookie("user_id", $id, time() + 60 * 60 * 24 * 30, '/', $cookiedomain);
								setcookie("pass", '', time() + 60 * 60 * 24 * 30, '/', $cookiedomain);
								//auth('user');

								createWidgets($id);
								addRights('{student}', '{community}', 1);
								addRights('{member}', '{community}', 1);

								$redirect_path = create_redirect();
								if ($redirect_path != '')
								{
									redirect($redirect_path);
								}
								else
								{
									redirect($server_absolute_path . 'socstart/');
								}
							}
							else
							{
								$auth_error = true;
							}
						}
					}
				}
				else
				{
					$auth_error = true;
				}
			}
			else
			{
				$auth_error = true;
			}
		}
		elseif (encode($_REQUEST[ 'error_description' ]) != '')
		{
			$auth_error = true;
		}
		else
		{
			$content2 .= '<div class="maincontent_data">
<div class="soc_part full">
...
' . $LOC[ 'redirect_to_auth' ] . '
</div>
</div>
<script>
	$(document).ready(function(){
		window.location = "' . SSO_APP_URL . '/oauth2/authorize?response_type=code&grant_type=authorization_code&client_id=' . SSO_APP_ID . '&redirect_uri=' . $server_absolute_path . 'sso_auth/";
	});
</script>';
		}
	}
	else
	{
		$user_data = getuser($_SESSION[ 'user_id' ]);
		if ($user_data[ 'sso' ] == '')
		{
			$content2 .= '<div class="maincontent_data">
<div class="soc_part full">
' . sprintf($LOC[ 'already_logged' ], $server_absolute_path . 'profile/') . '
</div>
</div>';
		}
		else
		{
			$redirect_path = create_redirect();
			if ($redirect_path != '')
			{
				redirect($redirect_path);
			}
			else
			{
				redirect($server_absolute_path . 'socstart/');
			}
		}
	}

	if ($auth_error)
	{
		$content2 .= '<div class="maincontent_data">
<div class="soc_part full">
' . $LOC[ 'couldnt_auth' ] . '<br><br>
' . $auth_error_description . '
</div>
</div>';
	}